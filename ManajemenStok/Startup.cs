﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ManajemenStok.Startup))]
namespace ManajemenStok
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
