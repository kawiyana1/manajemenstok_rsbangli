//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManajemenStok.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_ListOrderDetail
    {
        public decimal Order_ID { get; set; }
        public int Barang_ID { get; set; }
        public double Qty_Order { get; set; }
        public double Qty_RAP { get; set; }
        public double Qty_Tlh_Dibeli { get; set; }
        public double Qty_Pending { get; set; }
        public decimal Harga_Order { get; set; }
        public double Diskon_1 { get; set; }
        public Nullable<double> PPn { get; set; }
        public string No_Permintaan { get; set; }
        public double Qty_Penerimaan { get; set; }
        public string Kode_Pajak { get; set; }
        public Nullable<double> Rate_Pajak { get; set; }
        public string Status { get; set; }
        public string Kode_Satuan { get; set; }
        public Nullable<short> JenisBarangID { get; set; }
        public Nullable<decimal> Harga_Jual { get; set; }
        public decimal Urutan { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan { get; set; }
    }
}
