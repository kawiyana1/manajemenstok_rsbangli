//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManajemenStok.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_ListStokOpnameDetail
    {
        public string No_Bukti { get; set; }
        public int Barang_ID { get; set; }
        public string Kode_Satuan { get; set; }
        public Nullable<double> Stock_Akhir { get; set; }
        public double Qty_Opname { get; set; }
        public Nullable<decimal> Harga_Rata { get; set; }
        public string Keterangan { get; set; }
        public Nullable<short> JenisBarangID { get; set; }
        public Nullable<System.DateTime> Tgl_Expired { get; set; }
        public Nullable<double> Qty_Expired { get; set; }
        public string Nama_Barang { get; set; }
        public string Kode_Barang { get; set; }
        public Nullable<double> Konversi { get; set; }
        public string NamaSatuan { get; set; }
        public string Nama_Kategori { get; set; }
    }
}
