﻿using ManajemenStok.Entities.SIM;
using ManajemenStok.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Inventory.Controllers.Inventory
{
    public class InfoStatusStokController : Controller
    {
        private string tipepelayanan = "NONE";
        private string tipepelayanan2 = "NONE";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan && 
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_MinMaxStok.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value)
                            );
                        }
                        if (x.Key == "SubKategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _x = int.Parse(x.Value);
                            proses = proses.Where(y =>
                            y.SubKategori_ID == _x
                            );
                        }
                        if (x.Key == "LokasiIdFilter" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _x = int.Parse(x.Value);
                            proses = proses.Where(y =>
                            y.Lokasi_ID == _x
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang,
                        SubKategori_ID = m.SubKategori_ID,
                        Nama_Sub_Kategori = m.Nama_Sub_Kategori,
                        Max = m.Max_Stok,
                        Min = m.Min_Stok,
                        Stok = m.Stok,
                        Total = m.StokTotal,
                        Harga = m.Harga_Beli,
                        Lokasi_ID = m.Lokasi_ID
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

    }
}